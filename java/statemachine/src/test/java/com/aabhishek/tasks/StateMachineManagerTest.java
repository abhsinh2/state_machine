package com.aabhishek.tasks;

import java.io.File;
import java.util.Arrays;

import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.aabhishek.statemachine.StateMachineDao;
import com.aabhishek.statemachine.StateMachineDaoImpl;
import com.aabhishek.statemachine.StateMachineManager;
import com.aabhishek.statemachine.event.EventQueue;
import com.aabhishek.statemachine.listener.StateChangeEvent;
import com.aabhishek.statemachine.listener.StateChangeEventListener;
import com.aabhishek.statemachine.listener.StateChangeEventPublisher;
import com.aabhishek.statemachine.listener.TaskCompletionEvent;
import com.aabhishek.statemachine.listener.TaskCompletionEventListener;
import com.aabhishek.statemachine.listener.TaskCompletionEventPublisher;
import com.aabhishek.statemachine.model.StateMachine;

public class StateMachineManagerTest {
	@Test
	public void testStateMachine() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		StateMachine stateMachine = objectMapper.readValue(new File("src/test/resources/states.json"), StateMachine.class);
		
		EventQueue eventQueue = new EventQueue();
		StateChangeEventPublisher<String> stateChangeEventPublisher = new StateChangeEventPublisher<>(eventQueue);
		TaskCompletionEventPublisher<String> taskCompletionEventPublisher = new TaskCompletionEventPublisher<>(eventQueue);
		
		TaskManager taskManager = new TaskManager(Arrays.asList("com.aabhishek.tasks"));
		StateMachineDao<String> dao = new StateMachineDaoImpl<>();		
		
		StateMachineManager<String> stateMachineManager = new StateMachineManager<>(stateMachine, stateChangeEventPublisher, dao);
		
		StateChangeEventListener<String> stateEventListener = new StateChangeEventListener<>(taskManager, taskCompletionEventPublisher);	
		TaskCompletionEventListener<String> taskCompletionEventListener = new TaskCompletionEventListener<>(stateMachineManager);
		
		eventQueue.register(StateChangeEvent.class, stateEventListener);
		eventQueue.register(TaskCompletionEvent.class, taskCompletionEventListener);
		
		stateMachineManager.initialize("ElasticSearch.1");
		
		
		stateMachineManager.publish("local", "start", "alarm_index");
	}
}
