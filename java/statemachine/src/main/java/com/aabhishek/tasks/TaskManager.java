package com.aabhishek.tasks;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.reflections.Reflections;

public class TaskManager {
	private Map<String, Class<? extends Task<?, ?>>> tasks;
	
	public TaskManager(List<String> packages) {
		tasks = new HashMap<>();
		scan(packages);
	}
	
	private void scan(List<String> packages) {
		packages.forEach(name -> scan(name));
	}
	
	@SuppressWarnings("unchecked")
	private void scan(String packageName) {
		Reflections reflections = new Reflections(packageName);
		Set<Class<?>> types = reflections.getTypesAnnotatedWith(Register.class);
		for (Class<?> clazz : types) {			
			if (Task.class.isAssignableFrom(clazz)) {
				Register[] registers = clazz.getAnnotationsByType(Register.class);
				Register register = registers[0];
				for (RegisterState registerState: register.value()) {
					String key = this.createKey(registerState.source(), registerState.state());
					tasks.put(key, (Class<? extends Task<?, ?>>) clazz);
				}
			} else {
				System.out.println(String.format("%s in not assign", clazz));
			}
		}
	}
	
	private String createKey(String source, String state) {
		return String.format("%s#%s", source, state);
	}
	
	@SuppressWarnings("rawtypes")
	public Class<? extends Task> getTaskClass(String source, String state) {
		String key = this.createKey(source, state);
		return tasks.get(key);
	}
	
	@SuppressWarnings("rawtypes")
	public Task getTaskObject(String source, String state) {
		Class<? extends Task> taskClass = this.getTaskClass(source, state);
		try {
			return taskClass.getDeclaredConstructor().newInstance();
		} catch (Exception e) {
			return null;
		}
	}
}
