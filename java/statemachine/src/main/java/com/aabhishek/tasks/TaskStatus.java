package com.aabhishek.tasks;

public enum TaskStatus {
	SUCCESS("success"), FAILED("failed");
	
	private String name;
	
	TaskStatus(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
}
