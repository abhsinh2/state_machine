package com.aabhishek.tasks;

public class TaskResult<N> {
	private TaskStatus status;
	private N data;

	public TaskResult(TaskStatus status, N data) {
		super();
		this.status = status;
		this.data = data;
	}

	public TaskStatus getStatus() {
		return status;
	}

	public N getData() {
		return data;
	}

}
