package com.aabhishek.tasks;

public interface Task<T, N> {
	public TaskResult<N> execute(T t) throws TaskException;
}
