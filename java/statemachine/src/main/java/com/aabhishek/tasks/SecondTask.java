package com.aabhishek.tasks;

@Register({@RegisterState(source = "local", state = "Second")})
public class SecondTask extends BaseTask<String, String> {
	@Override
	public TaskResult<String> execute(String data) throws TaskException {
		System.out.println(String.format("Executing SecondTask"));
		return new TaskResult<String>(TaskStatus.SUCCESS, data);
	}
}
