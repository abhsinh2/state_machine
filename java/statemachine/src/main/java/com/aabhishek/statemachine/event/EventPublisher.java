package com.aabhishek.statemachine.event;

public interface EventPublisher<T> {
	public void publish(T event);
}
