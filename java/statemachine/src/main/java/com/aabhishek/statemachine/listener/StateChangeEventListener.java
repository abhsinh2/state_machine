package com.aabhishek.statemachine.listener;

import java.util.Map.Entry;

import com.aabhishek.statemachine.event.Event;
import com.aabhishek.statemachine.event.EventListener;
import com.aabhishek.tasks.Task;
import com.aabhishek.tasks.TaskException;
import com.aabhishek.tasks.TaskManager;
import com.aabhishek.tasks.TaskResult;
import com.aabhishek.tasks.TaskStatus;

public class StateChangeEventListener<T> implements EventListener<StateChangeEvent<T>>{
	private TaskManager taskManager;
	private TaskCompletionEventPublisher<T> publisher;
	
	public StateChangeEventListener(TaskManager taskManager, TaskCompletionEventPublisher<T> publisher) {
		this.taskManager = taskManager;
		this.publisher = publisher;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void listen(Event event) {
		StateChangeEvent<T> stateChangeEvent = (StateChangeEvent<T>) event;
		System.out.println(String.format("Received event %s", event));
		
		for (Entry<String, String> eachState : stateChangeEvent.getState().entrySet()) {
			if (!eachState.getKey().equals("local")) {
				continue;
			}
			Task<T, ?> task = this.taskManager.getTaskObject(eachState.getKey(), eachState.getValue());
			if (task == null) {
				System.out.println(String.format("No task found for %s", eachState));
				continue;
			}
			TaskResult<?> result = null;
			try {
				result = task.execute(stateChangeEvent.getData());
			} catch (TaskException e) {
				System.out.println(e);
				result = new TaskResult<T>(TaskStatus.FAILED, null);
			}
			publisher.publish(new TaskCompletionEvent(result.getStatus(), result.getData()));
		}
	}
}
