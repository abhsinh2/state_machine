package com.aabhishek.statemachine.listener;

import com.aabhishek.statemachine.event.Event;
import com.aabhishek.tasks.TaskStatus;

public class TaskCompletionEvent<T> implements Event {
	private TaskStatus status;
	private T data;
	
	public TaskCompletionEvent(TaskStatus status, T data) {
		this.status = status;
		this.data = data;
	}

	public TaskStatus getStatus() {
		return status;
	}

	public T getData() {
		return data;
	}

	@Override
	public String toString() {
		return "TaskCompletionEvent [status=" + status + ", data=" + data + "]";
	}
}
