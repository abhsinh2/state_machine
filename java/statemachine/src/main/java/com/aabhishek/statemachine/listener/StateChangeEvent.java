package com.aabhishek.statemachine.listener;

import java.util.Map;

import com.aabhishek.statemachine.event.Event;

public class StateChangeEvent<T> implements Event {
	private Map<String, String> state;
	private T data;
	
	public StateChangeEvent(Map<String, String> state, T data) {
		this.state = state;
		this.data = data;
	}

	public Map<String, String> getState() {
		return state;
	}

	public T getData() {
		return data;
	}

	@Override
	public String toString() {
		return "StateChangeEvent [state=" + state + ", data=" + data + "]";
	}
}
