package com.aabhishek.statemachine.listener;

import com.aabhishek.statemachine.event.EventPublisher;
import com.aabhishek.statemachine.event.EventQueue;

public class StateChangeEventPublisher<T> implements EventPublisher<StateChangeEvent<T>> {
	private EventQueue queue;
	
	public StateChangeEventPublisher(EventQueue queue) {
		this.queue = queue;
	}

	@Override
	public void publish(StateChangeEvent<T> event) {
		System.out.println(String.format("Publishing %s", event));
		this.queue.add(event);
	}

}
