package com.aabhishek.statemachine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class StateMachineDaoImpl<T> implements StateMachineDao<T> {
	private List<StateMachineDaoObject<T>> data;
	
	public StateMachineDaoImpl() {
		this.data = new ArrayList<>();
	}
	@Override
	public void save(StateMachineDaoObject<T> obj) throws StateMachineDaoException {
		this.data.add(obj);
	}

	@Override
	public Collection<StateMachineDaoObject<T>> load() throws StateMachineDaoException {
		return this.data;
	}

}
