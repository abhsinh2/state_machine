package com.aabhishek.statemachine.listener;

import com.aabhishek.statemachine.event.EventPublisher;
import com.aabhishek.statemachine.event.EventQueue;

public class TaskCompletionEventPublisher<T> implements EventPublisher<TaskCompletionEvent<T>> {
	private EventQueue queue;
	
	public TaskCompletionEventPublisher(EventQueue queue) {
		this.queue = queue;
	}

	@Override
	public void publish(TaskCompletionEvent<T> event) {
		System.out.println(String.format("Publishing %s", event));
		this.queue.add(event);
	}
}
