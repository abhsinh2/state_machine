package com.aabhishek.statemachine.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Event {
	private String name;
	private String source;
	
	@JsonCreator
	public Event(@JsonProperty("source") String source, @JsonProperty("name")String name) {
		this.name = name;
		this.source = source;
	}

	public String getName() {
		return name;
	}

	public String getSource() {
		return source;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, source);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		return Objects.equals(name, other.name) && Objects.equals(source, other.source);
	}

	@Override
	public String toString() {
		return "Event [name=" + name + ", source=" + source + "]";
	}
	
}
