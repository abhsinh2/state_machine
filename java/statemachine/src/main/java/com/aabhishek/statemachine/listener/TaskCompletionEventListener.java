package com.aabhishek.statemachine.listener;

import com.aabhishek.statemachine.StateMachineManager;
import com.aabhishek.statemachine.event.Event;
import com.aabhishek.statemachine.event.EventListener;

public class TaskCompletionEventListener<T> implements EventListener<TaskCompletionEvent<T>>{
	private StateMachineManager<T> stateMachineManager;
	
	public TaskCompletionEventListener(StateMachineManager<T> stateMachineManager) {
		this.stateMachineManager = stateMachineManager;
	}
	
	@SuppressWarnings("unchecked")
	public void listen(Event event) {
		TaskCompletionEvent<T> taskCompletionEvent = (TaskCompletionEvent<T>) event;
		System.out.println(String.format("Received event %s", event));
		stateMachineManager.publish("local", taskCompletionEvent.getStatus().getName(), taskCompletionEvent.getData());
	}
}
