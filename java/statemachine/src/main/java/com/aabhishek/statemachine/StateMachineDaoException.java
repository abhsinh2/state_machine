package com.aabhishek.statemachine;

public class StateMachineDaoException extends Exception {

	private static final long serialVersionUID = 1L;

	public StateMachineDaoException() {
		super();
	}

	public StateMachineDaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public StateMachineDaoException(String message) {
		super(message);
	}

	public StateMachineDaoException(Throwable cause) {
		super(cause);
	}

}
