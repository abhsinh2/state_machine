package com.aabhishek.statemachine.event;

import java.util.HashMap;
import java.util.Map;

public class EventQueue {
	private Map<Class<? extends Event>, EventListener<? extends Event>> listeners;
	
	public EventQueue() {
		listeners = new HashMap<>();
	}
	
	public void register(Class<? extends Event> clazz, EventListener<?> listener) {
		this.listeners.put(clazz, listener);
	}
	
	public void add(Event event) {
		EventListener<? extends Event> listener = this.listeners.get(event.getClass());
		if (listener != null) {
			listener.listen(event);
		}
	}
}
