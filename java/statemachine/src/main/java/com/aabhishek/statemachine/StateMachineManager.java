package com.aabhishek.statemachine;

import java.util.HashMap;
import java.util.Map;
import com.aabhishek.statemachine.listener.StateChangeEvent;
import com.aabhishek.statemachine.listener.StateChangeEventPublisher;
import com.aabhishek.statemachine.model.Event;
import com.aabhishek.statemachine.model.State;
import com.aabhishek.statemachine.model.StateMachine;
import com.aabhishek.statemachine.model.Transition;

public class StateMachineManager<T> {
	private Map<String, State> states;
	private State currentState;
	private StateChangeEventPublisher<T> publisher;
	private StateMachineDao<T> dao;
	
	public StateMachineManager(StateMachine stateMachine, StateChangeEventPublisher<T> publisher, StateMachineDao<T> dao) {
		this.states = new HashMap<>();
		this.build(stateMachine);
		this.publisher = publisher;
		this.dao = dao;
	}

	private void build(StateMachine stateMachine) {
		stateMachine.getStates().forEach(st -> {
			states.put(st.getId(), st);
		});
	}

	public void initialize(String id) {
		this.currentState = this.states.get(id);
	}

	public void publish(String source, String state, T data) {
		Transition transition = findTransition(source, state);
		if (transition == null) {
			System.out.println(String.format("No transition found from current state %s for source '%s', state '%s'",
					this.currentState.getId(), source, state));
			return;
		}
		System.out.println(String.format("Transitioning to %s", transition.getState()));
		
		this.currentState = this.states.get(transition.getState());
		try {
			this.dao.save(new StateMachineDaoObject<T>(this.currentState.getId(), data));
		} catch (StateMachineDaoException e) {
			System.out.println(e);
			return;
		}
		StateChangeEvent<T> stateChangeEvent = new StateChangeEvent<T>(this.currentState.getState(), data);
		this.publisher.publish(stateChangeEvent);		
	}

	private Transition findTransition(String source, String state) {
		for (Transition transition : this.currentState.getTransitions()) {
			if (transition.getEvent().equals(new Event(source, state))) {
				return transition;
			}
		}
		return null;
	}

}
