package com.aabhishek.statemachine.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class State {
	private String id;
	private Map<String, String> state;
	private List<Transition> transitions;

	@JsonCreator
	public State(@JsonProperty("id") String id, @JsonProperty("state") Map<String, String> state, @JsonProperty("transitions") List<Transition> transitions) {
		this.id = id;
		this.state = state;
		this.transitions = transitions;
	}

	public String getId() {
		return id;
	}

	public Map<String, String> getState() {
		return state;
	}

	public List<Transition> getTransitions() {
		return transitions;
	}

	@Override
	public String toString() {
		return "State [id=" + id + ", state=" + state + ", transitions=" + transitions + "]";
	}

}
