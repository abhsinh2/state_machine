package com.aabhishek.statemachine.event;

public interface EventListener<T extends Event> {
	public void listen(Event event);
}
