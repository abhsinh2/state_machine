package com.aabhishek.statemachine;

import java.util.Collection;

public interface StateMachineDao<T> {
	public void save(StateMachineDaoObject<T> obj) throws StateMachineDaoException;
	public Collection<StateMachineDaoObject<T>> load() throws StateMachineDaoException;
}
