package com.aabhishek.statemachine.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Transition {
	private String state;
	private Event event;
	
	@JsonCreator
	public Transition(@JsonProperty("state") String state, @JsonProperty("event") Event event) {
		super();
		this.state = state;
		this.event = event;
	}

	public String getState() {
		return state;
	}

	public Event getEvent() {
		return event;
	}

	@Override
	public String toString() {
		return "Transition [state=" + state + ", event=" + event + "]";
	}

}
