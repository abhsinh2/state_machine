package com.aabhishek.statemachine.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StateMachine {
	private String name;
	private List<State> states;
	
	@JsonCreator
	public StateMachine(@JsonProperty("name") String name, @JsonProperty("states") List<State> states) {
		this.name = name;
		this.states = states;
	}

	public String getName() {
		return name;
	}

	public List<State> getStates() {
		return states;
	}

	@Override
	public String toString() {
		return "StateMachine [name=" + name + ", states=" + states + "]";
	}
	
}
