package com.aabhishek.statemachine;

public class StateMachineDaoObject<T> {
	public String id;
	public T data;

	public StateMachineDaoObject(String id, T data) {
		this.id = id;
		this.data = data;
	}

	public T getData() {
		return data;
	}

	public String getId() {
		return id;
	}

}
